export const INVALID_REQUEST_ERROR = "Invalid Request";
export const INVALID_ACCESS_TOKEN = "Invalid access token";
export const INVALID_REFRESH_TOKEN = "Invalid refresh token";
export const UNKNOWN_ERROR_TRY_AGAIN =
  "Unknown error occured. Please try again.";
export const INVALID_CREDENTIAL_ERROR = "Invalid credential";

