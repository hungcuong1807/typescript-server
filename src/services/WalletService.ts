import { TYPES } from "../types";
import { injectable, inject } from "inversify";
import * as bip39 from "bip39";
import { derivePath } from "ed25519-hd-key";

import * as crypto from "crypto";

import { IWalletService } from "./interfaces/IWalletService";
import { Keypair, PublicKey } from "@solana/web3.js";
import { IWalletRepository } from "@repositories/interfaces/IWalletRepository";
import {
  AccountHistoryInfo,
  MainAccountInfo,
  TokenAccountInfo,
  TokenInfo,
} from "models/wallet/wallet.model";
import { InternalError } from "@errors/app.error";
import { IMarketRepository } from "@repositories/interfaces/IMarketRepository";

@injectable()
export default class WalletService implements IWalletService {
  @inject(TYPES.WalletRepository) private walletRepository!: IWalletRepository;

  @inject(TYPES.MarketRepository) private marketRepository!: IMarketRepository;
  createSeedPhraseWords(): string {
    const randomBytes = crypto.randomBytes(16);
    const mnemonic = bip39.entropyToMnemonic(randomBytes.toString("hex"));
    return mnemonic;
  }

  async createWallet(seedPharses: string): Promise<string> {
    const seed = await bip39.mnemonicToSeedSync(seedPharses);
    const path = `m/44'/501'/0'`;
    const keypair = Keypair.fromSeed(
      derivePath(path, seed.toString("hex")).key
    );
    if (process.env.BLOCKCHAIN_ENV === "devnet") {
      await this.walletRepository.requestAirDrop(keypair.publicKey);
    }
    return keypair.publicKey.toBase58();
  }

  async getMainAccountInfo(publicKey: string): Promise<MainAccountInfo> {
    const accountAddress = new PublicKey(publicKey);

    const accountInfo = await this.walletRepository.getMainAccountInfo(
      accountAddress
    );
    const ratioToUsd = await this.marketRepository.getRatioUsdtByToken(
      "solana"
    );
    accountInfo.ratioToUsd = `${ratioToUsd.currentUSDPrice} USD/1 SOL`;
    return accountInfo;
  }
  async getTokenAccountByOwner(
    ownerAddress: string
  ): Promise<TokenAccountInfo[]> {
    const accountAddress = new PublicKey(ownerAddress);
    const tokenLists = await this.walletRepository.getAllTokenOnNetwork(
      "devnet"
    );
    const tokenAccounts = (
      await this.walletRepository.getTokenAccountByOwner(accountAddress)
    ).map((item) => {
      const nameInfoToken = this.getInfoNameTokenByOwner(tokenLists, item.mint);
      return {
        ...item,
        name: nameInfoToken.name,
        symbol: nameInfoToken.symbol,
      };
    });
    return tokenAccounts;
  }
  async getTransactionHistoryAccount(
    address: string
  ): Promise<AccountHistoryInfo[]> {
    const accountAddress = new PublicKey(address);
    return await this.walletRepository.getTransactionHistoryAccount(
      accountAddress
    );
  }
  async transferNativeToken(
    toAddress: string,
    ammount: number
  ): Promise<string> {
    const toAddressAccount = new PublicKey(toAddress);
    return await this.walletRepository.transferNativeToken(
      toAddressAccount,
      ammount
    );
  }

  async transferTokenAccount(
    fromAddress: string,
    toAddress: string,
    ammount: number
  ): Promise<string> {
    const tokenAccountAddressFrom = new PublicKey(fromAddress);
    const tokenAccountAddressTo = new PublicKey(toAddress);
    const accountTokenFromInfo =
      await this.walletRepository.getTokenAccountInfo(tokenAccountAddressFrom);
    const accountTokenToInfo = await this.walletRepository.getTokenAccountInfo(
      tokenAccountAddressTo
    );
    if (!accountTokenToInfo || !accountTokenFromInfo) {
      throw new InternalError("Cannot find the address");
    }
    if (accountTokenFromInfo.mint !== accountTokenToInfo.mint) {
      throw new InternalError("The token accounts are not same mint address");
    }
    const signatureTransaction =
      await this.walletRepository.transferTokenAccount(
        tokenAccountAddressFrom,
        tokenAccountAddressTo,
        ammount,
        accountTokenFromInfo.tokenAmount.decimals
      );
    return signatureTransaction;
  }
  async getAllTokenOnNetwork(
    env: string,
    search?: string
  ): Promise<TokenInfo[]> {
    return await this.walletRepository.getAllTokenOnNetwork(env, search);
  }
  async getDetailTransaction(signature: string): Promise<any> {
    return await this.walletRepository.getDetailTransaction(signature);
  }

  private getInfoNameTokenByOwner(
    listTokens: TokenInfo[],
    mintAddress: string
  ): TokenInfo {
    const tokenInfos = listTokens.filter(
      (item) => item.address === mintAddress
    );
    if (tokenInfos.length) {
      return {
        name: tokenInfos[0].name,
        symbol: tokenInfos[0].symbol,
      };
    }
    return {
      name: "Unknown Token",
      symbol: "Unknown",
    };
  }
}
