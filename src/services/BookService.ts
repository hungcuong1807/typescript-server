import { NotFoundError } from "@errors/app.error";
import { IBookRepository } from "@repositories/interfaces/IBookRepository";
import { injectable, inject } from "inversify";
import { BookModel } from "models/book/book.model";
import { TYPES } from "../types";
import { IBookService } from "./interfaces/IBookService";

@injectable()
export default class BookService implements IBookService {
  @inject(TYPES.BookRepository) private bookRepository!: IBookRepository;

  async getAll(): Promise<BookModel[]> {
    return await this.bookRepository.getAll();
  }
  async getById(id: number): Promise<BookModel> {
    const book = await this.bookRepository.getById(id);
    if (!book || Object.keys(book).length === 0) {
      throw new NotFoundError("The book does not exist");
    }
    return book;
  }
  async updateById(id: number, data: Partial<BookModel>): Promise<boolean> {
    const isValidBook = await this.checkValidBook(id);
    if (isValidBook) {
      return await this.bookRepository.updateById(id, data);
    } else {
      throw new NotFoundError("The book does not exist");
    }
  }
  async create(data: Partial<BookModel>): Promise<boolean> {
    return await this.bookRepository.create(data);
  }

  private async checkValidBook(id: number): Promise<boolean> {
    const book = await this.bookRepository.getById(id);
    if (!book || Object.keys(book).length === 0) {
      return false;
    }
    return true;
  }
}
