import { BookModel } from "models/book/book.model";
import { IServiceBase } from "./IServiceBase";

export interface IBookService extends IServiceBase<BookModel> {}
