import {
  MainAccountInfo,
  TokenAccountInfo,
  AccountHistoryInfo,
  TokenInfo,
} from "models/wallet/wallet.model";

export interface IWalletService {
  createSeedPhraseWords(): string;
  createWallet(seedPharses: string): Promise<string>;

  getMainAccountInfo(publicKey: string): Promise<MainAccountInfo>;

  getTokenAccountByOwner(ownerAddress: string): Promise<TokenAccountInfo[]>;

  getTransactionHistoryAccount(address: string): Promise<AccountHistoryInfo[]>;

  transferNativeToken(toAddress: string, ammount: number): Promise<string>;

  transferTokenAccount(
    fromAddress: string,
    toAddress: string,
    ammount: number
  ): Promise<string>;

  getAllTokenOnNetwork(env: string, search?: string): Promise<TokenInfo[]>;

  getDetailTransaction(signature: string): Promise<any>;
}
