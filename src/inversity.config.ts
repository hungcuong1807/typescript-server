import { Container } from "inversify";
import { TYPES } from "./types";

import BookRepository from "@repositories/BookRepository";
import BookService from "@services/BookService";
import { IBookService } from "@services/interfaces/IBookService";
import { IBookRepository } from "@repositories/interfaces/IBookRepository";
import BookController from "@controllers/BookController/book.controller";
import { IWalletService } from "@services/interfaces/IWalletService";
import WalletService from "@services/WalletService";
import WalletController from "@controllers/WalletController/wallet.controller";
import { IWalletRepository } from "@repositories/interfaces/IWalletRepository";
import WalletRepository from "@repositories/WalletRepository";
import { IMarketRepository } from "@repositories/interfaces/IMarketRepository";
import MarketRepository from "@repositories/MarketRepository";

const container = new Container({ defaultScope: "Singleton" });

// book
container.bind(BookController).to(BookController);
container.bind<IBookRepository>(TYPES.BookRepository).to(BookRepository);
container.bind<IBookService>(TYPES.BookService).to(BookService);

//wallet
container.bind(WalletController).to(WalletController);
container.bind<IWalletRepository>(TYPES.WalletRepository).to(WalletRepository);
container.bind<IWalletService>(TYPES.WalletService).to(WalletService);

// market
container.bind<IMarketRepository>(TYPES.MarketRepository).to(MarketRepository);

export default container;
