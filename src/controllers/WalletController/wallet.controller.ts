import { BadRequestError } from "@errors/app.error";
import { IWalletService } from "@services/interfaces/IWalletService";
import {
  Request as ExpressRequest,
  Response as ExpressResponse,
} from "express";

import { injectable, inject } from "inversify";
import { TYPES } from "../../types";

@injectable()
export default class WalletController {
  @inject(TYPES.WalletService) private walletService!: IWalletService;

  public async createSeedPhraseWords(
    _req: ExpressRequest,
    res: ExpressResponse
  ) {
    const mnemonic = await this.walletService.createSeedPhraseWords();
    res.send({ data: mnemonic });
  }

  public async createWallet(req: ExpressRequest, res: ExpressResponse) {
    const publicKey = await this.walletService.createWallet(
      req.body.wordPhrases
    );
    res.send({ publicKey });
  }

  public async getBalanceAccountInfo(
    req: ExpressRequest,
    res: ExpressResponse
  ) {
    if (!req.params.address) {
      throw new BadRequestError("address is missing");
    }
    const accountInfo = await this.walletService.getMainAccountInfo(
      req.params.address
    );
    res.send({ data: accountInfo });
  }

  public async getTokenAccountByOwner(
    req: ExpressRequest,
    res: ExpressResponse
  ) {
    if (!req.params.address) {
      throw new BadRequestError("address is missing");
    }
    const tokenAccounts = await this.walletService.getTokenAccountByOwner(
      req.params.address
    );
    res.send({ data: tokenAccounts });
  }

  public async getHistoryTransactionsAccount(
    req: ExpressRequest,
    res: ExpressResponse
  ) {
    if (!req.params.address) {
      throw new BadRequestError("address is missing");
    }
    const historyTransactions =
      await this.walletService.getTransactionHistoryAccount(req.params.address);
    res.send({ data: historyTransactions });
  }

  public async transferNativeToken(req: ExpressRequest, res: ExpressResponse) {
    const signatureTransaction = await this.walletService.transferNativeToken(
      req.body.toAddress,
      req.body.ammount
    );
    res.send({ data: { signature: signatureTransaction } });
  }

  public async transferTokenAccount(req: ExpressRequest, res: ExpressResponse) {
    const signatureTransaction = await this.walletService.transferTokenAccount(
      req.body.fromAddress,
      req.body.toAddress,
      req.body.ammount
    );
    res.send({ data: { signature: signatureTransaction } });
  }

  public async getAllTokenOnNetwork(req: ExpressRequest, res: ExpressResponse) {
    const tokenList = await this.walletService.getAllTokenOnNetwork(
      req.body.env,
      req.body.search
    );
    res.send({ data: tokenList });
  }

  public async getDetailTransaction(req: ExpressRequest, res: ExpressResponse) {
    if (!req.params.signature) {
      throw new BadRequestError("signature transaction is missing");
    }
    const detailTransaction = await this.walletService.getDetailTransaction(
      req.params.signature
    );
    res.send({ data: detailTransaction });
  }
}
