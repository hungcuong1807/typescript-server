import { Application } from "express";
import container from "../../inversity.config";
import WalletController from "@controllers/WalletController/wallet.controller";
import { asyncWrap } from "@utils/asyncWrapper";
import {
  transferNativeTokenValidator,
  transferTokenAccountValidator,
  walletCreationModelValidator,
  listTokenValidator,
} from "@validation/walletValidator";

export default function (app: Application) {
  const WalletControllerInstance =
    container.get<WalletController>(WalletController);

  // wallet controller
  app.get(
    "/wallet/createphrase",
    asyncWrap(
      WalletControllerInstance.createSeedPhraseWords.bind(
        WalletControllerInstance
      )
    )
  );
  app.post(
    "/wallet/createwallet",
    walletCreationModelValidator,
    asyncWrap(
      WalletControllerInstance.createWallet.bind(WalletControllerInstance)
    )
  );

  app.get(
    "/wallet/getbalanceaccount/:address?",
    asyncWrap(
      WalletControllerInstance.getBalanceAccountInfo.bind(
        WalletControllerInstance
      )
    )
  );
  app.get(
    "/wallet/gettokenaccounts/:address?",
    asyncWrap(
      WalletControllerInstance.getTokenAccountByOwner.bind(
        WalletControllerInstance
      )
    )
  );

  app.get(
    "/wallet/gethistorytransaction/:address?",
    asyncWrap(
      WalletControllerInstance.getHistoryTransactionsAccount.bind(
        WalletControllerInstance
      )
    )
  );

  app.post(
    "/wallet/transfernative",
    transferNativeTokenValidator,
    asyncWrap(
      WalletControllerInstance.transferNativeToken.bind(
        WalletControllerInstance
      )
    )
  );

  app.post(
    "/wallet/transfertokenaccount",
    transferTokenAccountValidator,
    asyncWrap(
      WalletControllerInstance.transferTokenAccount.bind(
        WalletControllerInstance
      )
    )
  );

  app.get(
    "/wallet/getalltokens",
    listTokenValidator,
    asyncWrap(
      WalletControllerInstance.getAllTokenOnNetwork.bind(
        WalletControllerInstance
      )
    )
  );

  app.get(
    "/wallet/getdetailtransaction/:signature?",
    asyncWrap(
      WalletControllerInstance.getDetailTransaction.bind(
        WalletControllerInstance
      )
    )
  );
}
