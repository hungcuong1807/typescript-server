import { BadRequestError, MissingFieldError } from "@errors/app.error";
import BookService from "@services/BookService";
import {
  Request as ExpressRequest,
  Response as ExpressResponse,
} from "express";

import { injectable, inject } from "inversify";
import { BookModel } from "models/book/book.model";
import { TYPES } from "../../types";

@injectable()
export default class BookController {
  @inject(TYPES.BookService) private bookService!: BookService;

  public async getAll(_req: ExpressRequest, res: ExpressResponse) {
    const books = await this.bookService.getAll();
    res.send({ data: books });
  }

  public async getBookById(req: ExpressRequest, res: ExpressResponse) {
    if (!req.params.id) {
      throw new BadRequestError("id");
    }
    const book = await this.bookService.getById(Number(req.params.id));
    res.send({ data: book });
  }

  public async createBook(req: ExpressRequest, res: ExpressResponse) {
    if (req.body.book) {
      const bookCreationModel = req.body.book as BookModel;
      await this.bookService.create(bookCreationModel);
      res.send({ data: true });
    }
  }

  public async updateBookId(req: ExpressRequest, res: ExpressResponse) {
    if (!req.params.id) {
      throw new MissingFieldError("id");
    }
    if (req.body.book) {
      const bookUpdate = req.body.book as BookModel;
      await this.bookService.updateById(Number(req.params.id), bookUpdate);
      res.send({ data: true });
    }
  }
}
