import { Application } from "express";
import { asyncWrap } from "@utils/asyncWrapper";
import BookController from "@controllers/BookController/book.controller";

import container from "../../inversity.config";
import { bookCreationModelValidator } from "@validation/bookValidator";

export default function (app: Application) {
  const BookControllerInstance = container.get<BookController>(BookController);
  // book controller
  app.get(
    "/books",
    asyncWrap(BookControllerInstance.getAll.bind(BookControllerInstance))
  );
  app.get(
    "/book/:id?",
    asyncWrap(BookControllerInstance.getBookById.bind(BookControllerInstance))
  );
  app.post(
    "/books",
    bookCreationModelValidator,
    asyncWrap(BookControllerInstance.createBook.bind(BookControllerInstance))
  );
  app.patch(
    "/book/:id?",
    asyncWrap(BookControllerInstance.updateBookId.bind(BookControllerInstance))
  );
}
