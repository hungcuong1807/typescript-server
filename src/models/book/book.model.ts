export interface BookModel {
  id?: number;
  author: string;
  name: string;
}
