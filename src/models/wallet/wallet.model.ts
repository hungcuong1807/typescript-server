import { TransactionError } from "@solana/web3.js";

export interface MainAccountInfo {
  executable?: boolean;
  lamports?: number;
  rentEpoch?: number;
  balance?: number;
  address?: string;
  ratioToUsd?: string;
}

export interface TokenAmmount {
  ammount: string;
  decimals: number;
  uiAmmount: number;
  uiAmmountString: string;
}

export interface TokenAccountInfo {
  address?: string;
  isNative: boolean;
  mint: string;
  owner: string;
  state: string;
  tokenAmount: TokenAmmount;
  name?: string;
  symbol?: string;
}

export interface AccountHistoryInfo {
  err: TransactionError | null;
  signature: string;
  slot: number;
  memo: string | null;
  blockTime: number | string;
}

export interface TokenInfo {
  name?: string;
  symbol?: string;
  decimals?: number;
  address?: string;
}
export interface MarketRatioUSDToken {
  currentUSDPrice: number;
  marketcapRank: number;
}
