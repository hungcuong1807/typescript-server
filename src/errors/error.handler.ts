import {
  Application,
  Request as ExpressRequest,
  Response as ExpressResponse,
  NextFunction,
} from "express";
import { NotFoundError, ApplicationError } from "./app.error";
import logger from "../logger";

export default function (app: Application) {
  app.use(() => {
    throw new NotFoundError("This route is not correct");
  });

  // Log all errors
  app.use(function (
    err: any,
    req: ExpressRequest,
    _res: ExpressResponse,
    next: NextFunction
  ) {
    if (err instanceof Error) {
      logger.error(
        `${req.method} ${req.path}: Unhandled request error. ${err.message}`
      );
    } else if (typeof err === "string") {
      logger.error(
        `${req.method} ${req.path}: Unhandled request error. ${err}`
      );
    }

    next(err);
  });
  // request error handler
  app.use(function onError(
    err: ApplicationError,
    _req: ExpressRequest,
    res: ExpressResponse,
    _: NextFunction
  ) {
    if (err.message) {
      logger.error(err.message);
      return res
        .status(err.code)
        .send({ errorCode: err.code, message: err.message });
    } else {
      return res.sendStatus(err.code);
    }

    // next(err);
  });

  // Optional fallthrough error handler
  // app.use(function onError(
  //   err: any,
  //   _req: ExpressRequest,
  //   res: ExpressResponse,
  //   _next: NextFunction
  // ) {
  //   if (err instanceof ApplicationError) {
  //     console.log(err.code);
  //   }
  //   res.statusCode = 500;

  //   res.end(err.message + "\n");
  // });
}
