import { injectable } from "inversify";
import { IWalletRepository } from "./interfaces/IWalletRepository";
import { connectionDevNet } from "../databases/ConnectionBlockChain";
import {
  LAMPORTS_PER_SOL,
  PublicKey,
  SendTransactionError,
  SystemProgram,
  Transaction,
} from "@solana/web3.js";
import * as SPLToken from "@solana/spl-token";
import { TokenListProvider } from "@solana/spl-token-registry";

import { InternalError } from "@errors/app.error";
import {
  AccountHistoryInfo,
  MainAccountInfo,
  TokenAccountInfo,
  TokenInfo,
} from "models/wallet/wallet.model";

import { ALICE_ACCOUNT } from "@repositories/configs/account_transfer";
import logger from "../logger";
@injectable()
export default class WalletRepository implements IWalletRepository {
  async getMainAccountInfo(publicKey: PublicKey): Promise<MainAccountInfo> {
    const balance = await connectionDevNet.getBalance(publicKey);
    const accountInfo = await connectionDevNet.getAccountInfo(publicKey);
    let accountResult: MainAccountInfo = {};
    accountResult.balance = balance / LAMPORTS_PER_SOL;
    accountResult.executable = accountInfo?.executable;
    accountResult.lamports = accountInfo?.lamports;
    accountResult.rentEpoch = accountInfo?.rentEpoch;
    accountResult.address = publicKey.toBase58();
    return accountResult;
  }

  async getTokenAccountInfo(
    address: PublicKey
  ): Promise<TokenAccountInfo | undefined> {
    try {
      const tokenAccount = await connectionDevNet.getParsedAccountInfo(address);
      if (tokenAccount.value?.data && "parsed" in tokenAccount.value?.data) {
        let infoToken = tokenAccount.value?.data.parsed[
          "info"
        ] as TokenAccountInfo;
        infoToken.address = address.toBase58();
        return infoToken;
      }
      return undefined;
    } catch (error: any) {
      throw new InternalError("Cannot find the address token account");
    }
  }
  async requestAirDrop(publicKey: PublicKey): Promise<boolean> {
    try {
      let airdropSignature = await connectionDevNet.requestAirdrop(
        publicKey,
        LAMPORTS_PER_SOL
      );
      await connectionDevNet.confirmTransaction(airdropSignature);
      return true;
    } catch (err: any) {
      if (err instanceof SendTransactionError) {
        throw new InternalError(err.logs!.join(" "));
      }
      throw new InternalError("Somethings went wrong");
    }
  }

  async getTokenAccountByOwner(
    address: PublicKey
  ): Promise<TokenAccountInfo[]> {
    const tokenAccountsByOwner =
      await connectionDevNet.getParsedTokenAccountsByOwner(address, {
        programId: SPLToken.TOKEN_PROGRAM_ID,
      });
    if (tokenAccountsByOwner.value.length) {
      return tokenAccountsByOwner.value.map((item) => {
        let infoToken = item.account.data.parsed["info"] as TokenAccountInfo;
        infoToken.address = item.pubkey.toBase58();
        return infoToken;
      });
    }
    return [];
  }

  async getTransactionHistoryAccount(
    address: PublicKey
  ): Promise<AccountHistoryInfo[]> {
    try {
      const historyTransactions =
        (await connectionDevNet.getSignaturesForAddress(address, {
          limit: 25,
        })) as AccountHistoryInfo[];
      return historyTransactions;
    } catch (e: any) {
      logger.error(`get transaction history failed: ${e as string}`);
      throw new InternalError(e.message);
    }
  }

  async transferNativeToken(
    toAddress: PublicKey,
    ammount: number
  ): Promise<string> {
    try {
      const tx = new Transaction().add(
        SystemProgram.transfer({
          fromPubkey: ALICE_ACCOUNT.publicKey,
          toPubkey: toAddress,
          lamports: ammount * LAMPORTS_PER_SOL,
        })
      );
      const signatureTransaction = await connectionDevNet.sendTransaction(tx, [
        ALICE_ACCOUNT,
      ]);
      // confirm transaction signature
      await connectionDevNet.confirmTransaction(signatureTransaction);
      return signatureTransaction;
    } catch (error: any) {
      if (error instanceof SendTransactionError) {
        throw new InternalError(error.logs!.join("\n"));
      }
      throw new InternalError("Somethings went wrong");
    }
  }
  async transferTokenAccount(
    fromAddress: PublicKey,
    toAddress: PublicKey,
    ammount: number,
    decimals: number
  ): Promise<string> {
    try {
      const decimalAmmounts = new Array(decimals).fill(0);
      const ammountTransfer =
        decimals > 0
          ? Number(`${ammount}${decimalAmmounts.join("")}`)
          : ammount;
      const tx = new Transaction().add(
        SPLToken.Token.createTransferInstruction(
          SPLToken.TOKEN_PROGRAM_ID, // always token program address
          fromAddress, // from (token account public key)
          toAddress, // to (token account public key)

          ALICE_ACCOUNT.publicKey, // from's authority
          [], // pass signer if from's mint is a multisig pubkey
          ammountTransfer // amount
        )
      );
      const signatureTransaction = await connectionDevNet.sendTransaction(tx, [
        ALICE_ACCOUNT,
      ]);
      await connectionDevNet.confirmTransaction(signatureTransaction);
      return signatureTransaction;
    } catch (err: any) {
      if (err instanceof SendTransactionError) {
        throw new InternalError(err.logs!.join("\n"));
      }
      throw new InternalError("Somethings went wrong");
    }
  }
  async getAllTokenOnNetwork(
    env: string,
    search?: string
  ): Promise<TokenInfo[]> {
    const tokens = await new TokenListProvider().resolve();
    let tokenList = tokens
      .filterByClusterSlug(env)
      .getList()
      .map((item) => ({
        name: item.name,
        address: item.address,
        decimals: item.decimals,
        symbol: item.symbol,
      }));
    if (search && tokenList.length > 0) {
      tokenList = tokenList.filter(
        (item) =>
          new RegExp(`${search}`, "i").test(item.name) ||
          new RegExp(`${search}`, "i").test(item.symbol)
      );
    }
    return tokenList;
  }

  async getDetailTransaction(signature: string): Promise<any> {
    return await connectionDevNet.getConfirmedTransaction(signature);
  }
}
