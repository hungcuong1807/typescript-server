import { injectable, unmanaged } from "inversify";
import { IRepositoryBase } from "@repositories/interfaces/IReposityBase";
import db from "../databases/Database";
import { InternalError } from "@errors/app.error";

@injectable()
export default class RepositoryBase<T> implements IRepositoryBase<T> {
  private readonly tableName: string;
  constructor(@unmanaged() tableName: string) {
    this.tableName = tableName;
  }
  public async getAll(): Promise<T[]> {
    return new Promise((resolve) => {
      db.all(`SELECT * FROM ${this.tableName}`, [], (err: Error, rows: any) => {
        if (err) {
          throw new InternalError(err.message);
        } else {
          resolve(rows as T[]);
        }
      });
    });
  }
  public getById(id: number): Promise<T> {
    return new Promise((resolve) => {
      db.get(
        `SELECT * FROM ${this.tableName} WHERE id = ?`,
        [id],
        (err: Error, result: any) => {
          if (err) {
            throw new InternalError(err.message);
          } else {
            resolve(result as T);
          }
        }
      );
    });
  }
  public updateById(id: number, data: Partial<T>): Promise<boolean> {
    let paramString: string[] = [];
    let paramValues: any[] = [];
    Object.keys(data).forEach((item) => {
      paramString.push(`${item} = ?`);
      paramValues.push(data[item as keyof Partial<T>]);
    });
    return new Promise((resolve) => {
      db.run(
        `UPDATE ${this.tableName} SET ${paramString.join(", ")} WHERE id = ?`,
        [...paramValues, id],
        (err: Error) => {
          if (err) {
            throw new InternalError(err.message);
          } else {
            resolve(true);
          }
        }
      );
    });
  }
  public create(data: Partial<T>): Promise<boolean> {
    let params: any[] = [];
    let fields: string[] = [];
    Object.keys(data).forEach((item) => {
      fields.push(item);
      params.push(data[item as keyof Partial<T>]);
    });
    return new Promise((resolve) => {
      db.run(
        `INSERT INTO ${this.tableName} (${fields.join(
          ","
        )}) VALUES (${new Array(fields.length).fill("?")})`,
        params,
        (err: Error) => {
          if (err) {
            throw new InternalError(err.message);
          } else {
            resolve(true);
          }
        }
      );
    });
  }
}
