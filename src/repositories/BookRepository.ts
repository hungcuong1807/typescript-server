import { injectable } from "inversify";
import { BookModel } from "models/book/book.model";
import { IBookRepository } from "./interfaces/IBookRepository";
import RepositoryBase from "./ReposityBase";

@injectable()
export default class BookRepository
  extends RepositoryBase<BookModel>
  implements IBookRepository
{
  constructor() {
    super("Book");
  }
}
