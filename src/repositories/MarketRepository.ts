import { MarketRatioUSDToken } from "models/wallet/wallet.model";
import { IMarketRepository } from "./interfaces/IMarketRepository";
import axios from "axios";
import { coingeckoAPI } from "../databases/ConnectionBlockChain";
import { injectable } from "inversify";

@injectable()
export default class MarketRepository implements IMarketRepository {
  async getRatioUsdtByToken(tokenId: string): Promise<MarketRatioUSDToken> {
    try {
      const response = await axios.get(`${coingeckoAPI}/${tokenId}`);
      const tokenMarketInfo = response.data;
      return {
        currentUSDPrice: tokenMarketInfo.market_data.current_price.usd,
        marketcapRank: tokenMarketInfo.market_cap_rank,
      };
    } catch (error: any) {
      return {
        currentUSDPrice: 0,
        marketcapRank: 0,
      };
    }
  }
}
