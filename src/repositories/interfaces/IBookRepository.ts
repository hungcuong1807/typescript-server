import { BookModel } from "models/book/book.model";
import { IRepositoryBase } from "./IReposityBase";

export interface IBookRepository extends IRepositoryBase<BookModel>{
    
}