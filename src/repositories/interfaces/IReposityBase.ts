export interface IRepositoryBase<T> {
  getAll(): Promise<T[]>;
  getById(id: number): Promise<T>;
  updateById(id: number, data: Partial<T>): Promise<boolean>;
  create(data: Partial<T>): Promise<boolean>;
}
