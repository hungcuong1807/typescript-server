import { PublicKey } from "@solana/web3.js";
import {
  MainAccountInfo,
  TokenAccountInfo,
  AccountHistoryInfo,
  TokenInfo,
} from "models/wallet/wallet.model";
export interface IWalletRepository {
  requestAirDrop(publicKey: PublicKey): Promise<boolean>;
  // getBalanceAccount(publicKey: PublicKey): Promise<number>;
  getMainAccountInfo(address: PublicKey): Promise<MainAccountInfo>;

  getTokenAccountByOwner(address: PublicKey): Promise<TokenAccountInfo[]>;

  getTransactionHistoryAccount(
    address: PublicKey
  ): Promise<AccountHistoryInfo[]>;

  transferNativeToken(toAddress: PublicKey, ammount: number): Promise<string>;

  transferTokenAccount(
    fromAddress: PublicKey,
    toAddress: PublicKey,
    ammount: number,
    decimals: number
  ): Promise<string>;

  getTokenAccountInfo(
    address: PublicKey
  ): Promise<TokenAccountInfo | undefined>;

  getAllTokenOnNetwork(env: string, search?: string): Promise<TokenInfo[]>;

  getDetailTransaction(signature: string): Promise<any>;
}
