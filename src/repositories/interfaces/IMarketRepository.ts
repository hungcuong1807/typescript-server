import { MarketRatioUSDToken } from "models/wallet/wallet.model";

export interface IMarketRepository {
  getRatioUsdtByToken(tokenId: string): Promise<MarketRatioUSDToken>;
}
