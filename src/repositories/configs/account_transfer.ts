import { Keypair } from "@solana/web3.js";
import * as bip39 from "bip39";
const seed = bip39.mnemonicToSeedSync(
  process.env.ALICE_SEED_WORDS || "",
  process.env.ALICE_PASSWORD
);
export const ALICE_ACCOUNT = Keypair.fromSeed(seed.slice(0, 32));
