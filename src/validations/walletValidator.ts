import { body } from "express-validator";
import {
  Request as ExpressRequest,
  Response as ExpressResponse,
  NextFunction,
} from "express";
import * as bip39 from "bip39";
import { handleValidatorMessage } from "@utils/handleValidator";

export const walletCreationModelValidator = [
  body("wordPhrases")
    .custom((value) => {
      return bip39.validateMnemonic(value);
    })
    .withMessage("seed words errors"),
  (req: ExpressRequest, res: ExpressResponse, next: NextFunction) => {
    handleValidatorMessage(req, res, next);
  },
];

export const transferNativeTokenValidator = [
  body("toAddress")
    .notEmpty()
    .withMessage("The transfered account should not be empty"),
  (req: ExpressRequest, res: ExpressResponse, next: NextFunction) => {
    handleValidatorMessage(req, res, next);
  },
  body("ammount").isNumeric().withMessage("The ammount should be numeric"),
  (req: ExpressRequest, res: ExpressResponse, next: NextFunction) => {
    handleValidatorMessage(req, res, next);
  },
];

export const transferTokenAccountValidator = [
  body("fromAddress")
    .notEmpty()
    .withMessage("The transfered account should not be empty"),
  (req: ExpressRequest, res: ExpressResponse, next: NextFunction) => {
    handleValidatorMessage(req, res, next);
  },
  body("toAddress")
    .notEmpty()
    .withMessage("The transfered account should not be empty"),
  (req: ExpressRequest, res: ExpressResponse, next: NextFunction) => {
    handleValidatorMessage(req, res, next);
  },
  body("ammount").isNumeric().withMessage("The ammount should be numeric"),
  (req: ExpressRequest, res: ExpressResponse, next: NextFunction) => {
    handleValidatorMessage(req, res, next);
  },
];

export const listTokenValidator = [
  body("env")
    .notEmpty()
    .custom(
      (value) =>
        value === "devnet" || value === "testnet" || value === "mainnet-beta"
    )
    .withMessage(
      "Enter valid enviroment network, should be devnet, testnet or mainnet-beta"
    ),
  (req: ExpressRequest, res: ExpressResponse, next: NextFunction) => {
    handleValidatorMessage(req, res, next);
  },
];
