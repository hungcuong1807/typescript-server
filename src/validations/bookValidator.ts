import { body } from "express-validator";
import {
  Request as ExpressRequest,
  Response as ExpressResponse,
  NextFunction,
} from "express";
import { handleValidatorMessage } from "@utils/handleValidator";

export const bookCreationModelValidator = [
  body("book.author").notEmpty().withMessage("The author should not be empty"),
  (req: ExpressRequest, res: ExpressResponse, next: NextFunction) => {
    handleValidatorMessage(req, res, next);
  },
  body("book.name").notEmpty().withMessage("The name of book should not be empty"),
  (req: ExpressRequest, res: ExpressResponse, next: NextFunction) => {
    handleValidatorMessage(req, res, next);
  },
];
