import sqlite3 from "sqlite3";

const db = new sqlite3.Database("./example.db", (err: Error | null) => {
  if (err) {
    console.log("Could not connect to database", err);
  } else {
    console.log("Connected to database");
  }
});

// function createTable() {
//   console.log("createTable Book");
//   db.run("DELETE FROM Book");
//   db.run(
//     "CREATE TABLE IF NOT EXISTS Book(id INTEGER PRIMARY KEY AUTOINCREMENT,author text NOT NULL, name text NOT NULL)",
//     insertRows
//   );
// }
// function insertRows() {
//   console.log("insert rows");
//   db.run(
//     "INSERT INTO Book (id,author,name) VALUES(1,'Viktor','The darknight');"
//   );
//   db.run(
//     "INSERT INTO Book (id,author,name) VALUES(2,'Finn','Ke huy diet vu tru');"
//   );
//   db.run(
//     "INSERT INTO Book (id,author,name) VALUES(3,'Brown','Train to Busan');"
//   );
//   db.run(
//     "INSERT INTO Book (id,author,name) VALUES(4,'Bruno','Batman is comming');"
//   );
//   db.close();
// }

// createTable();
export default db;
