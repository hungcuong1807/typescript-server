import { Connection, clusterApiUrl } from "@solana/web3.js";

export const connectionDevNet = new Connection(clusterApiUrl("devnet"));
export const connectionMainNet = new Connection(clusterApiUrl("mainnet-beta"));

// use coingecko like 
export const coingeckoAPI = "https://api.coingecko.com/api/v3/coins";