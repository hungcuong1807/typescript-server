// types for inversity config

export const TYPES = {
  // type for Base controller, service and repo
  RepositoryBase: Symbol("RepositoryBase"),
  //   ServiceBase: Symbol("ServiceBase"),

  // type for Book controller, service and repo
  BookService: Symbol("BookService"),
  BookController: Symbol("BookController"),
  BookRepository: Symbol("BookRepository"),

  // type for Wallet controller, service and repo
  WalletService: Symbol("WalletService"),
  WalletController: Symbol("WalletController"),
  WalletRepository: Symbol("WalletRepository"),

  // type for Market
  MarketRepository: Symbol("MarketRepository"),
};
