import "module-alias/register";
import { install as installSourceMapSupport } from "source-map-support";
installSourceMapSupport();
import "reflect-metadata";
import express from "express";
import compress from "compression";
import server from "./server";
import cors from "cors";
import routeBooks from "@controllers/BookController/routes";
import routeWallets from "@controllers/WalletController/routes";
import errorHandler from "./errors/error.handler";
import logger from "./logger";
import bodyParser from "body-parser";

async function bootstrap() {
  server.disable("x-powered-by"); // Hide information
  server.use(compress()); // Compress
  server.use(cors());
  server.use(bodyParser.json());
  server.use(bodyParser.urlencoded({ extended: true }));

  server.use(express.json());
  server.use(express.urlencoded({ extended: true }));

  // add routes
  routeBooks(server);
  routeWallets(server);

  // configure error
  errorHandler(server);
}
// Need for integration testing
export default server;
// Invoking the bootstrap function
bootstrap()
  .then(() => {
    logger.info("Server is up");
  })
  .catch((error) => {
    logger.error("Unknown error. " + error.message);
  });
