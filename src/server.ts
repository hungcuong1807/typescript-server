import express from "express";
import dotenv from "dotenv";
import logger from "./logger";
dotenv.config();

const server = express();
const PORT = process.env.PORT || 3000;

server.listen(PORT, () => {
  logger.info(`Running Node.js version ${process.version}`);
  logger.info(`App environment: ${process.env.NODE_ENV}`);
  logger.info(`App is running on port ${PORT}`);
});

export default server;
