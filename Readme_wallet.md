# Các phương thức trong wallet solana
### Tạo 12 từ seed phrase
* /wallet/createphrase: Tạo ra 12 từ bất kì sử dụng thư viện bip39


### Tạo địa chỉ ví
* /wallet/createwallet: Tạo địa chỉ ví, sử dụng 12 từ + derivation path để tạo ra seed => Keypair. Có thể check trên solana explorer, hoặc import address vào các ví phantom.  

```
const seed = await bip39.mnemonicToSeedSync(seedPharses);
    const path = `m/44'/501'/0'`;
    const keypair = Keypair.fromSeed(
      derivePath(path, seed.toString("hex")).key
    );
```
Thường thì từ client sẽ tạo ra seed từ 12 từ, rồi gửi seed.toString("hex") từ client lên server. Tuy nhiên demo sẽ gửi 12 từ lên

### Lấy balance của 1 địa chỉ
* /wallet/getbalanceaccount/:address? Lấy balance của 1 địa chỉ

### Lấy các tokens của 1 địa chỉ
* /wallet/gethistorytransaction/:address?
Trên mạng solana, cũng giống như eth, sẽ có những token được tạo ra từ những mint address, để giao dịch các token đó, thì ví của mình cần có địa chỉ liên kết vs các mint address đó.

```
const tokenAccountsByOwner =
      await connectionDevNet.getParsedTokenAccountsByOwner(address, {
        programId: SPLToken.TOKEN_PROGRAM_ID,
      });
    if (tokenAccountsByOwner.value.length) {
      return tokenAccountsByOwner.value.map((item) => {
        let infoToken = item.account.data.parsed["info"] as TokenAccountInfo;
        infoToken.address = item.pubkey.toBase58();
        return infoToken;
      });
    }
```

### Transfer native token
* /wallet/transfernative
Trong thực tế, client sẽ tạo signature của transaction rồi, gửi lên server signature đó để confirm. Ở demo, ta fix cứng account dùng để chuyển token và chỉ nhập account cần nhận token

```
  const tx = new Transaction().add(
        SystemProgram.transfer({
          fromPubkey: ALICE_ACCOUNT.publicKey,
          toPubkey: toAddress,
          lamports: ammount * LAMPORTS_PER_SOL,
        })
      );
      const signatureTransaction = await connectionDevNet.sendTransaction(tx, [
        ALICE_ACCOUNT,
      ]);
```
### Transfer token account
* /wallet/transfertokenaccount
Gửi 2 token không phải native, ta cần check địa chỉ gửi token và địa chỉ được gửi token có cùng mint address, sao đó giao dịch bình thường, nếu có lỗi sẽ từ mạng solana throw

```
const decimalAmmounts = new Array(decimals).fill(0);
      const ammountTransfer =
        decimals > 0
          ? Number(`${ammount}${decimalAmmounts.join("")}`)
          : ammount;
      const tx = new Transaction().add(
        SPLToken.Token.createTransferInstruction(
          SPLToken.TOKEN_PROGRAM_ID, // always token program address
          fromAddress, // from (token account public key)
          toAddress, // to (token account public key)

          ALICE_ACCOUNT.publicKey, // from's authority
          [], // pass signer if from's mint is a multisig pubkey
          ammountTransfer // amount
        )
      );
      const signatureTransaction = await connectionDevNet.sendTransaction(tx, [
        ALICE_ACCOUNT,
      ]);
```

### Lấy danh sách các token
* /wallet/getalltokens: Trả về name, symbol, decimals và address (truyền vào env network như là devnet, mainnet-beta, testnet. Ngoài ra có thể sử dụng tên hoặc symbol của token, thông qua biến search)

### Lấy danh sách lịch sử giao dịch
* /wallet/gethistorytransaction/:address?
Trả về các signature của các history
```
  const historyTransactions =
        (await connectionDevNet.getSignaturesForAddress(address, {
          limit: 25,
        })) as AccountHistoryInfo[];
```

### Lấy chi tiết của 1 giao dịch
* /wallet/gethistorytransaction/:address?
Sử dụng signature của 1 giao dich, thì sẽ lấy đc chi tiết của 1 giao dịch

```
return await connectionDevNet.getConfirmedTransaction(signature);
```

### Script to test solana blockchain
* các operator: tạo mint address (để issue 1 token), tạo account liên quan đến mint address để giao dịch token, ...

[Script test]https://gitlab.com/hungcuong1807/solana-test

### Update naming a token
* sau khi tạo được các token thông qua các mint address, chúng ta có thể naming token bằng cách submit vào official repo của solana. Các bước được giới thiệu ở trang
https://learn.figment.io/tutorials/sol-mint-token